# How to get a working copy

* Checkout the repo
* `stack setup` (will install the correct version of GHC)
* `stack build` (will install the dependencies, and build the project)

# Usage

* `./$bin` where `$bin` is the executable outputted by the build
