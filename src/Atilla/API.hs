{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeFamilies #-}

module Atilla.API where

import Servant

import Atilla.Model

type API =
  "users" :> Get '[JSON] [User]
