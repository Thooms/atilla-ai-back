{-# LANGUAGE DeriveGeneric #-}

module Atilla.Model where

import GHC.Generics

import Data.Aeson

data User =
  User
  { ldapLogin :: String
  , firstName :: String
  , lastName :: String
  } deriving Generic

instance ToJSON User
