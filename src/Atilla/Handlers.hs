module Atilla.Handlers where

import Control.Monad.Trans.Either
import Servant

import Atilla.API
import Atilla.Model

-- Dummy handler for now
getUsers :: EitherT ServantErr IO [User]
getUsers = return $ [User "knuthd" "Donald" "Knuth"]


-- The main handler is a composition of smaller ones
mainHandler :: Server API
mainHandler = getUsers
