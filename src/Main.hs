{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeFamilies  #-}

module Main where

import Network.Wai
import Network.Wai.Handler.Warp
import Servant

import Atilla.API
import Atilla.Handlers

boilerplate :: Proxy API
boilerplate = Proxy

app :: Application
app = serve boilerplate mainHandler

main :: IO ()
main = run 8042 app
